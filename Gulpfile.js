/**
 * For perf benchmark run "gulp webperf".
 * The webperf task needs phantomas to be installed globally.
 * sudo npm install --global phantomas
 *
 * @see https://github.com/macbre/phantomas
 */

// Require all tasks in gulp/tasks, including subfolders.
var requireDir = require('require-dir');
requireDir('./gulp/tasks', { recurse: true });
