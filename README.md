# Stimul skeleton
Base template for html projects.

## Install
Install nodejs and bower.
For sprite generation install:
- PhantomJS
- brew install imagemagick

## Sprites
There are 2 gulp tasks to generate sprites:

- gulp sprites
- gulp svg-sprites

## Branches:
master (grunt & bower config, directory structure)
- drupal (drupal 7 base theme)
- wordpress
- drupal8
- html (simple html project)

## CSS3 easing

    .my-element
      transition: all 1.0s $easeOutCubic

## Available easing functions

    $linear
    $ease (default)
    $ease-in
    $ease-out
    $ease-in-out

    $easeInQuad
    $easeInCubic
    $easeInQuart
    $easeInQuint
    $easeInSine
    $easeInExpo
    $easeInCirc
    $easeInBack

    $easeOutQuad
    $easeOutCubic
    $easeOutQuart
    $easeOutQuint
    $easeOutSine
    $easeOutExpo
    $easeOutCirc
    $easeOutBack

    $easeInOutQuad
    $easeInOutCubic
    $easeInOutQuart
    $easeInOutQuint
    $easeInOutSine
    $easeInOutExpo
    $easeInOutCirc
    $easeInOutBack
