<?php
// Generated from: gulp/utils/config.assets.tpl.php

define('ASSETS_DEV', 'dev');
define('ASSETS_PROD', 'prod');

class AssetManager {
  public static $assets = array(
    'dist/boot.js' => 'dist/boot-30bcbcf574.js',
    'dist/site.css' => 'dist/site-b79fa8c607.css',
    'dist/svg/grunticon.loader.js' => 'dist/svg/grunticon-01357efef3.loader.js',
    'dist/vendors/ConsoleDummy.min.js' => 'dist/vendors/ConsoleDummy-90e05c842e.min.js',
    'dist/vendors/array.indexof.polyfill.js' => 'dist/vendors/array-9bc2c7d378.indexof.polyfill.js',
    'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js' => 'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js',
    'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js' => 'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js',
    'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js' => 'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js',
    'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js' => 'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js',
    'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js' => 'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js',
    'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js' => 'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js',
    'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js' => 'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js',
    'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js' => 'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js',
    'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js' => 'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js',
    'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js' => 'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js',
    'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd.min.js' => 'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd-cfc727b1bd.min.js',
    'dist/vendors/audiojs/audiojs/audio-cfc727b1bd.min.js' => 'dist/vendors/audiojs/audiojs/audio-cfc727b1bd-cfc727b1bd.min.js',
    'dist/vendors/audiojs/audiojs/audio.min.js' => 'dist/vendors/audiojs/audiojs/audio-cfc727b1bd.min.js',
    'dist/vendors/classList.js' => 'dist/vendors/classList-37058aca55.js',
    'dist/vendors/es5-sham.min.js' => 'dist/vendors/es5-sham-e66af4880c.min.js',
    'dist/vendors/es5-shim.min.js' => 'dist/vendors/es5-shim-9df24faab7.min.js',
    'dist/vendors/infobox.js' => 'dist/vendors/infobox-8a4283e0c0.js',
    'dist/vendors/modernizr.js' => 'dist/vendors/modernizr-b3fa0b437a.js',
    'dist/vendors/picturefill.min.js' => 'dist/vendors/picturefill-835c72599d.min.js',
    'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978.js' => 'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978.js',
    'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978.js' => 'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978.js',
    'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978.js' => 'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978.js',
    'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978.js' => 'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978.js',
    'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978.js' => 'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978.js',
    'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978.js' => 'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978.js',
    'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978.js' => 'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978.js',
    'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978-2967257978.js' => 'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978-2967257978-2967257978.js',
    'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978.js' => 'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978-2967257978.js',
    'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978.js' => 'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978-2967257978.js',
    'dist/vendors/plyr/plyr-2967257978-2967257978.js' => 'dist/vendors/plyr/plyr-2967257978-2967257978-2967257978.js',
    'dist/vendors/plyr/plyr-2967257978.js' => 'dist/vendors/plyr/plyr-2967257978-2967257978.js',
    'dist/vendors/plyr/plyr.js' => 'dist/vendors/plyr/plyr-2967257978.js',
    'dist/vendors/rAF.js' => 'dist/vendors/rAF-812881a966.js',
    'dist/vendors/respond.matchmedia.addListener.min.js' => 'dist/vendors/respond-87c090d540.matchmedia.addListener.min.js',
    'dist/vendors/respond.min.js' => 'dist/vendors/respond-a2684e9b8f.min.js',
    'dist/vendors/toastr/toastr.min.js' => 'dist/vendors/toastr/toastr-b36f28de58.min.js',
  );
}

function get_asset($asset) {
  if (defined('ASSETS_MODE') && ASSETS_MODE == ASSETS_PROD) {
    if (isset(AssetManager::$assets[$asset])) {
      return AssetManager::$assets[$asset];
    }
  }
  return $asset;
}
