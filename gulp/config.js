var theme_dir = process.cwd();

module.exports = {
  dist: {
    src: [
      theme_dir + '/dist/**/*.js',
      theme_dir + '/dist/*.css',
      // Don't add rev hash to *.bundle since they already have a hash
      // from webpack.
      '!' + theme_dir + '/dist/*.bundle.js'
    ],
    base: theme_dir + '/dist',
    dest: 'dist'
  },
  clean: {
    src: ['dist/*.css', 'dist/*.js', 'dist/vendors/*.js', 'dist/svg/grunticon-*.js']
  },
  images: {
    src: ['./assets/images/**/*.png', './assets/images/**/*.jpg', './assets/images/**/*.svg'],
    dest: './assets/images'
  },
  scripts: {
    lint: {
      src: [
        'src/*.js',
        'src/{modules,plugins}/**/*.js'
      ]
    },
    vendors: {
      src: ['src/vendors/**/*.js', '!src/vendors/bower_components/**'],
      dest: 'dist/vendors'
    },
    webpack: {
      context: theme_dir + '/src',
      entry: {
        boot: './boot.js'
      },
      output: {
        path: theme_dir + '/dist',
        filename: 'boot.js',
        chunkFilename: '[chunkhash].bundle.js'
      },
      resolve: {
        root: [theme_dir + '/src', theme_dir + '/src/vendors/bower_components']
      }
    }
  }
};
