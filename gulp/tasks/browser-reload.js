var gulp = require('gulp');
var browserSync = require('browser-sync');

gulp.task('browser-reload', function() {
    return browserSync.reload();
});

gulp.task('browser-sync', function() {
    return browserSync({
        proxy: 'localhost/stimbox.shop/',
        notify: false
    });
});