var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('default', function(callback) {
  runSequence(['scripts:dev', 'styles:dev'], 'browser-sync', 'watch', callback);
});
