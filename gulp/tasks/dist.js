var gulp = require('gulp');
var gutil = require('gulp-util');
var rev = require('gulp-rev');
var template = require('gulp-template');
var rename = require('gulp-rename');
var fs = require('fs');
var _ = require('lodash');
var config = require('../config').dist;
var runSequence = require('run-sequence');

var get_manifest = function(path) {
  var manifest;
  if (fs.existsSync(path)) {
    manifest = require('../../' + path);
    return manifest;
  }
  return {};
};

gulp.task('generateConfigAssetsPhp', function() {
  var data;
  data = {
    assets: {}
  };
  _.extend(data.assets, get_manifest('dist/rev-manifest.json'));
  return gulp.src('gulp/utils/config.assets.tpl.php')
    .pipe(rename('config.assets.php'))
    .pipe(template(data))
    .pipe(gulp.dest('./'));
});

gulp.task('copy-webpack-bundles', function() {
  // Need to copy directly the chunks created by webpack
  // since they already have a hash genrated in their filename.
  return gulp.src(['dist/*.bundle.js'])
    .pipe(gulp.dest('dist/'));
});

gulp.task('copy-async-loaded-scripts', function() {
  // Need to copy infobox.js since it's asynchronously loaded.
  return gulp.src(['src/vendors/infobox.js'])
    .pipe(gulp.dest('dist/vendors/'));
});


gulp.task('build-rev', function() {
  return gulp.src(config.src, {
      base: config.base
    }).pipe(rev())
    .pipe(gulp.dest(config.dest))
    .pipe(rev.manifest())
    .pipe(gulp.dest('dist'));
});

gulp.task('build', function(callback) {
  var done = function() {
    gutil.log(gutil.colors.white.bgGreen.bold('### Build complete ###'));
    callback();
  };
  return runSequence('clean', ['styles:prod', 'scripts:prod', 'svg-sprites-generate'], ['copy-webpack-bundles', 'copy-async-loaded-scripts'], 'build-rev', 'generateConfigAssetsPhp', done);
});
