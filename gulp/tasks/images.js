var gulp = require('gulp');
var newer = require('gulp-newer');
var imagemin = require('gulp-imagemin');
var config = require('../config').images;

gulp.task('images', function() {
  return gulp.src(config.src)
    // .pipe(newer(config.dest))
    .pipe(imagemin({
      progressive: true,
      interlaced: true,
      svgoPlugins: [{removeViewBox: false}]
    }))
    .pipe(gulp.dest(config.dest));
});
