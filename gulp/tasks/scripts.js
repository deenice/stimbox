var gulp = require('gulp');
var gutil = require('gulp-util');
var template = require('gulp-template');
var rename = require('gulp-rename');
var browserSync = require('browser-sync');
var newer = require('gulp-newer');
var webpack = require('webpack');
//var eslint = require('gulp-eslint');
var del = require('del');
var config = require('../config').scripts;
var runSequence = require('run-sequence');

var getWebpackConfig = function() {
  return {
    context: config.webpack.context,
    entry: config.webpack.entry,
    devtool: 'source-map',
    externals: [
      {
        jquery: 'jQuery'
      }
    ],
    output: config.webpack.output,
    module: {
      loaders: [
        {
          test: /\.js$/,
          exclude: [/bower_components/, /vendors/, /node_modules/],
          // add optional=jscript for ie8 compatibility with the mangle option.
          loader: 'babel',
          query: {
            // https://github.com/babel/babel-loader#options
            cacheDirectory: true,
            // loose: 'all',
            // externalHelpers: true,
            // http://babeljs.io/docs/usage/options/
            presets: ['es2015']
          }
        }, {
          test: /\.tpl.html$/,
          loader: 'mustache'
        }
      ]
    },
    resolve: config.webpack.resolve,
    plugins: [
      new webpack.ResolverPlugin(new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin('bower.json', ['main'])),
      new webpack.optimize.DedupePlugin()
    ]
  };
};

var compile_scripts = function(target, cb) {
  if (target === null) {
    target = 'dev';
  }
  var conf = getWebpackConfig();
  if (target === 'prod') {
    // Disable source-maps on prod.
    conf.devtool = false;
  }
  if (target === 'prod') {
    conf.plugins.push(new webpack.optimize.UglifyJsPlugin({
      mangle: true, // add mangle for better compression since we added optional=jscript for ie8 compat.
      output: {
        comments: false
      },
      compress: {
        drop_debugger: false,
        sequences: true,
        conditionals: true,
        booleans: true,
        unused: false,
        if_return: true,
        join_vars: true,
        drop_console: false,
        warnings: false
      }
    }));
  }
  return webpack(conf, function(err, stats) {
    if (err) {
      cb();
      gutil.log(gutil.colors.white.bgRed.bold('### Scripts error ###'));
      throw new gutil.PluginError('webpack', err);
    }
    gutil.log('[webpack]', stats.toString({
      chunks: false
    }));
    if (target !== 'prod') {
      browserSync.reload();
    }
    return cb();
  });
};

// gulp.task('lint-scripts', function() {
//   return gulp.src(config.lint.src)
//     .pipe(eslint())
//     .pipe(eslint.formatEach('stylish', process.stderr));
// });

gulp.task('scripts:clean', function(cb) {
  return del(['dist/*.bundle.js', 'dist/*.bundle.js.map'], cb);
});

gulp.task('scripts:dev', [], function(cb) {
  return runSequence('scripts:clean', ['copyVendors', 'generate-boot-js'], function() {
    compile_scripts('dev', cb);
  });
});

gulp.task('scripts:prod', function(cb) {
  return runSequence('scripts:clean', ['copyVendors', 'generate-boot-js'], function() {
    compile_scripts('prod', cb);
  });
});

gulp.task('copyVendors', function() {
  return gulp.src(config.vendors.src)
    .pipe(newer(config.vendors.dest))
    .pipe(gulp.dest(config.vendors.dest));
});

gulp.task('generate-boot-js', function() {
  var package_json_file = '../../package.json';
  delete require.cache[require.resolve(package_json_file)];
  var pkg = require(package_json_file);
  var components = [];
  for (var key in pkg.components) {
    var value = pkg.components[key];
    if (value === true) {
      components.push(key);
    }
  }
  return gulp.src('gulp/utils/boot.tpl.js')
    .pipe(rename('boot.js'))
    .pipe(template({
      components: components
    }))
    .pipe(gulp.dest('src'));
});
