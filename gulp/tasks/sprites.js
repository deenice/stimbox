var gulp = require('gulp');
var spritesmith = require('gulp.spritesmith');
var imagemin = require('gulp-imagemin');
var uuid = require('node-uuid');
var del = require('del');
var buffer = require('vinyl-buffer');

gulp.task('sprites:remove', function(cb) {
  return del(['assets/images/_ui-*.png'], cb);
});

gulp.task('sprites', ['sprites:remove'], function() {
  var cssSpriteFile = '_sprites__data.scss';
  var padding = 10;
  var image_id = uuid.v4();
  var image_name = '_ui-' + image_id + '-';

  var retinaSpriteData = gulp.src('./assets/images/ui@2x/*.png').pipe(spritesmith({
    imgName: image_name + '@2x.png',
    cssName: cssSpriteFile,
    padding: padding * 2,
    cssFormat: 'scss_maps',
    algorithm: 'top-down',
    algorithmOpts: {sort: false}
  }));

  retinaSpriteData.img.pipe(buffer()).pipe(imagemin({
    optimizationLevel: 5,
    progressive: true,
    interlaced: true
  })).pipe(gulp.dest('./assets/images/'));

  var spriteData = gulp.src('./assets/images/ui/*.png').pipe(spritesmith({
    imgName: image_name + '.png',
    cssName: cssSpriteFile,
    padding: padding,
    cssFormat: 'scss_maps',
    algorithm: 'top-down',
    algorithmOpts: {sort: false},
    cssOpts: {
      variableNameTransforms: []
    }
  }));

  spriteData.img.pipe(buffer()).pipe(imagemin({
    optimizationLevel: 5,
    progressive: true,
    interlaced: true
  })).pipe(gulp.dest('./assets/images/'));

  return spriteData.css.pipe(gulp.dest('./src/base/'));
});
