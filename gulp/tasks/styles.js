var gulp = require('gulp');
var gutil = require('gulp-util');
var browserSync = require('browser-sync');
var template = require('gulp-template');
var rename = require('gulp-rename');
// var changed = require('gulp-changed');
var filter = require('gulp-filter');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var gulpif = require('gulp-if');
var nano = require('cssnano');
var postcss = require('gulp-postcss');
var types = sass.compiler.types;

var compile_styles = function(target) {
  var dest = 'dist/';
  var is_prod = false;
  var imagePath = '../assets/images';
  if (target === null) {
    target = 'dev';
  }

  if (target === 'prod') {
    // no need for other dest, the build task copies from tmp/ to dist when
    // doing rev(). @see dist.js.
    // dest = 'dist/';
    is_prod = true;
  }
  var is_dev = !is_prod;

  var onError = function(err) {
    // change absolute path to relative to theme src/.
    err.file = err.file.replace(process.cwd() + '/src/', '');

    var html = '<strong>Sass compilation error</strong><br>';
    html += '<p>' + err.message + '<p>';
    html += '<p><strong>' + err.line + ':' + err.column + '</strong> - ' + err.file + '</p>';

    // sass.logError(err);
    var message = new gutil.PluginError('sass', err.messageFormatted).toString();
    process.stderr.write(message + '\n');

    browserSync.notify(html, 10000);
    this.emit('end');
  };

  return gulp.src('src/*.sass')
    .pipe(gulpif(is_dev, sourcemaps.init()))
    // .pipe(changed(dest, {extension: '.css'}))
    .pipe(sass({
      indentedSyntax: true,
      imagePath: imagePath,
      sourceComments: false,
      sourceMap: false,
      omitSourceMapUrl: true,
      functions: {
        'image-url($img)': function(img) {
          return new types.String('url("' + imagePath + '/' + img.getValue() + '")');
        }
      }
    }).on('error', onError))
    .pipe(gulpif(is_dev, sourcemaps.write({includeContent: false})))
    .pipe(gulpif(is_dev, sourcemaps.init({loadMaps: true})))
    .pipe(postcss([
      autoprefixer({
        browsers: ['> 1%', 'last 2 versions', 'ie 8', 'ie 9', 'ie 10', 'ios 6', 'android 4'],
        cascade: false
      }),
      nano({
        safe: true,
        discardComments: {removeAll: true},
        autoprefixer: false
      })
    ]))
    .pipe(gulpif(is_dev, sourcemaps.write('.')))
    .pipe(gulp.dest(dest))
    .pipe(filter('**/*.css'))
    .pipe(browserSync.reload({stream: true}));
};

gulp.task('styles:dev', ['generate-sass-extensions'], function(cb) {
  return compile_styles('dev', cb);
});

gulp.task('styles:prod', ['generate-sass-extensions'], function(cb) {
  return compile_styles('prod', cb);
});

gulp.task('generate-sass-extensions', function() {
  var package_json_file = '../../package.json';
  delete require.cache[require.resolve(package_json_file)];
  var pkg = require(package_json_file);

  return gulp.src('gulp/utils/_load-enabled-extensions.tpl.sass')
    .pipe(rename('_load-enabled-extensions.sass'))
    .pipe(template({
      components: pkg.components
    }))
    .pipe(gulp.dest('src/plugins'));
});
