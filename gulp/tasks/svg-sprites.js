var glob = require("glob");
var gulp = require("gulp");
var gulpicon = require("gulpicon/tasks/gulpicon");
var runSequence = require('run-sequence');

// grab the config, tack on the output destination
var config = require("../svg-config.js");
config.dest = "dist/svg";
config.template = 'gulp/utils/gulpicon-template.hbs';

// grab the file paths
var files = glob.sync("assets/images/svg/*.svg");

// set up the gulp task
gulp.task("svg-sprites-generate", gulpicon(files, config));

gulp.task('svg-sprites', function(callback) {
  return runSequence('svg-sprites-generate', callback);
});
