var gulp = require('gulp');
var batch = require('gulp-batch');
var browserSync = require('browser-sync');
var watch = require('gulp-watch');

gulp.task('build-styles-and-scripts', ['scripts:dev', 'styles:dev']);

gulp.task('watch', function() {
    watch([
        '*.php',
        '*.theme',
        'functions/*.php',
        'partials/*.php',
        'templates/*.twig',
        'templates/**/*.twig',
        '*/*.twig',
        '*/*.html.twig',
        'layouts/**/*',
        '!src/vendors/bower_components/**',
        'assets/images/**'
    ], batch(function(events, cb) {
        browserSync.reload();
        cb();
    }));

    /*watch(['assets/images/svg/*.svg'], batch(function(events, done) {
      gulp.start('svg-sprites', done);
    }));*/
    watch(['src/**/*.{sass,scss}', 'src/**/**/*.{sass,scss}', '!src/plugins/_load-enabled-extensions.sass'], batch(function(events, done) {
        gulp.start('styles:dev', done);
    }));
    watch(['src/{modules,plugins}/**/*.{js,html,txt}', 'src/*.{js,html,txt}', '!src/boot.js'], batch(function(events, done) {
        gulp.start('scripts:dev', done);
    }));
    watch(['package.json', 'gulp/utils/boot.tpl.js'], batch(function(events, done) {
        gulp.start('build-styles-and-scripts', done);
    }));
    watch(['src/vendors/*.js'], batch(function(events, done) {
        gulp.start('copyVendors', done);
    }));
});
