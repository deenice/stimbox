var gulp = require('gulp');

gulp.task('webperf', function(cb) {
  var phantomas = require('phantomas');
  var url = 'http://localhost/site_name';
  var options = {
    'analyze-css': true
  };

  phantomas(url, options, function(err, json, results) {
    if (err) {
      console.log("Error");
      console.log(err);
    }
    console.log('phantomas result');
    console.log(json);
    cb()
  });
});
