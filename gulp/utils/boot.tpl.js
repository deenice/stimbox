<%// Template for src/scripts/boot.js
%>/*eslint-disable */
// Generated from: gulp/utils/boot.tpl.js
// Set webpack public path for async loading.
__webpack_public_path__ = window.themeUrl + 'dist/';

// Import main components.
// import 'babel-core/external-helpers';
import 'vendors/ConsoleDummy.min';<% _.forEach(components, function(component) { %>
import <%- component %> from 'plugins/<%- component %>/<%- component %>';<% }); %>
import Site from 'site.js';

// Load extra modules
<% _.forEach(components, function(component) { %>new <%- component %>();
<% }); %>new Site();
