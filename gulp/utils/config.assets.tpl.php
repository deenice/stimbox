<?php
<%// Template for config.assets.php
%>// Generated from: gulp/utils/config.assets.tpl.php

define('ASSETS_DEV', 'dev');
define('ASSETS_PROD', 'prod');

class AssetManager {
  public static $assets = array(<% _.forEach(assets, function(asset, key) { %>
    'dist/<%- key %>' => 'dist/<%- asset %>',<% }); %>
  );
}

function get_asset($asset) {
  if (defined('ASSETS_MODE') && ASSETS_MODE == ASSETS_PROD) {
    if (isset(AssetManager::$assets[$asset])) {
      return AssetManager::$assets[$asset];
    }
  }
  return $asset;
}
