/*eslint-disable */
// Generated from: gulp/utils/boot.tpl.js
// Set webpack public path for async loading.
__webpack_public_path__ = window.themeUrl + 'dist/';

// Import main components.
// import 'babel-core/external-helpers';
import 'vendors/ConsoleDummy.min';
import Lightbox from 'plugins/Lightbox/Lightbox';
import MenuToggle from 'plugins/MenuToggle/MenuToggle';
import Slideshow from 'plugins/Slideshow/Slideshow';
import SmoothAnchors from 'plugins/SmoothAnchors/SmoothAnchors';
import Site from 'site.js';

// Load extra modules
new Lightbox();
new MenuToggle();
new Slideshow();
new SmoothAnchors();
new Site();
