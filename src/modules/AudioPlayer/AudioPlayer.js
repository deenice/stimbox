/**
 * Example:
 * <div data-slideshow> ... <div><img ...></div> ... </div>
 * Some configuration can be passed with data-* attributes on the .bxslider
 * data-fade: 'true', 'false' (default false)
 * data-arrows: 'true', 'false' (default true)
 * data-infinite: 'true', 'false' (default true)
 * data-dots: 'true', 'false' (default false)
 * data-autoplay: 'true', 'false' (default false)
 * data-autoplaySpeed: milliseconds before auto next slide (default 4000)
 *
 * example:
 * <div data-slideshow data-fade="true" data-dots="true">
 */

import $ from 'jquery';

export default class AudioPlayer {

  constructor(el) {
    this.$el = $(el);

    this.$el.data('audio-player', this);
    this.autostart = this.$el.data('autostart') || false;
    this.id = this.$el.data('id');
    const $audio = this.$el.find('audio:first');
    this.player = plyr.setup($audio[0], {});

    let $icon = $('[data-audio-player-id="'+this.id+'"]');
    this.$icon = $icon;
    $icon.on('click', (e) => {
      e.preventDefault();
      this.play();
    });

  }

  play() {
    const player = this.player[0];
    player.togglePlay();
    player.on('play', () => {
      this.$icon.addClass('active');
      this.$el.addClass('av__slide-player--visible');
    });
    player.on('pause stop', () => {
      this.$icon.removeClass('active');
      this.$el.removeClass('av__slide-player--visible');
    });
  }

  stop() {
    this.player[0].stop();
  }
}
