/**
 * Example:
 * <div data-slideshow> ... <div><img ...></div> ... </div>
 * Some configuration can be passed with data-* attributes on the .bxslider
 * data-fade: 'true', 'false' (default false)
 * data-arrows: 'true', 'false' (default true)
 * data-infinite: 'true', 'false' (default true)
 * data-dots: 'true', 'false' (default false)
 * data-autoplay: 'true', 'false' (default false)
 * data-autoplaySpeed: milliseconds before auto next slide (default 4000)
 *
 * example:
 * <div data-slideshow data-fade="true" data-dots="true">
*/

import $ from 'jquery';

export default class Timer {

  constructor(el) {
    this.$el = $(el);

    this.$el.data('timer', this);
    this.time = this.$el.data('time') || 30000;
    this.remainingTime = this.$el.data('remaining-time-alert') || Math.round(this.time * 0.166667, 0);
    this.perimeter = this.$el.data('perimeter') || 660;
    this.type = this.$el.data('type') || 'line';
    this.autostart = this.$el.data('autostart') || false;

    if (this.autostart) {
      this.start();
    }
  }

  start() {
    let i = 1;
    if(this.type == 'circle'){
      // http://jsfiddle.net/wz32sy7y/558/
      this.interval = setInterval(() => {
        if (i == this.time) {
          clearInterval(this.interval);
          return;
        }
        if(i == this.time - this.remainingTime){
          this.openLightbox('#popupSlideTimer');
        }
        this.$el.find('.circle_animation').css('stroke-dashoffset', this.perimeter-((i+1)*(this.perimeter/this.time)));
        i++;
      }, 1000);
    } else {
      this.interval = setInterval(() => {
        if (i == this.time) {
          clearInterval(this.interval);
          return;
        }
        if(i == this.time - this.remainingTime){
          this.openLightbox('#popupMainTimer');
        }
        let percent = i / this.time * 100;
        this.$el.find('.av__timer-elapsed').css('width', percent + '%');
        i++;
      }, 1000);
    }
  }

  stop() {
    this.$el.find('.circle_animation').css('stroke-dashoffset', this.perimeter);
    clearInterval(this.interval);
    this.interval = false;
  }

  openLightbox( src ){
    $.magnificPopup.open({
      items: {
        src: src
      },
      type: 'inline',
      mainClass: 'mfp-fade',
      closeMarkup: '<button type="button" title="%title%" class="mfp-close">Close</button>'
    });
  }

}
