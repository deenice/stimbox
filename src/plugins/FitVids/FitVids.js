// Example:
// <div data-fitvids> ... video ... </div>

import $ from 'jquery';
import 'fitvids';

export default class FitVids {
  constructor() {
    $('[data-fitvids]').fitVids({
      customSelector: 'iframe[src*="dailymotion.com"]'
    });
  }
}
