import $ from 'jquery';

export default {
  $html: $('html'),
  $body: $('body'),
  $htmlbody: $('html, body'),
  $window: $(window)
};
