/**
 * Example:
 * <div class="google-map" data-gmap
 * data-zoom="16" data-lat="46.7943077" data-long="7.15445"
 * data-saturation="-80" data-hue="#00d4ff"
 * data-marker="My label" data-locations=".block-members__item"
 * data-marker-filename="marker.png"
 * data-autoclose-info="false">
 * </div>
 *
 * For each location, you can specify the marker with data-marker-filename attribute
 * <div class=".block-members__item" data-lat=""
 *      data-lng="" data-marker-filename="filename.png"></div>
 *
 * All markers images have to be in images folder
 */

import $ from 'jquery';
const loadJS = window.loadJS;

window.googleMapLoaded = function() {
  loadJS(`${window.themeUrl}dist/vendors/infobox.js`, () => {
    const InfoBox = window.InfoBox;
    $('[data-gmap]').each(function() {
      const $this = $(this);
      const markers = [];
      let mapCenter = false;

      if ($this.data('lat') && $this.data('long')) {
        const lat = parseFloat($this.data('lat'));
        const long = parseFloat($this.data('long'));
        mapCenter = new google.maps.LatLng(lat, long);
      }
      else {
        mapCenter = new google.maps.LatLng(-34.397, 150.644);
      }

      const mapOptions = {
        zoom: $this.data('zoom') || 8,
        center: mapCenter,
        scrollwheel: false,
        disableDefaultUI: true
      };

      // Add simple saturation and hue map style.
      if ($this.data('saturation') || $this.data('hue')) {
        const styles = [];
        if ($this.data('saturation')) {
          styles.push({saturation: $this.data('saturation')});
        }
        if ($this.data('hue')) {
          styles.push({hue: $this.data('hue')});
        }
        mapOptions.styles = [{stylers: styles}];
      }

      if ($('html').hasClass('touch')) {
        mapOptions.draggable = false;
      }

      const map = new google.maps.Map($this.get(0), mapOptions);

      const infowindow = new InfoBox({
        boxClass: 'infobox',
        boxStyle: {
          width: '485px'
        },
        disableAutoPan: false,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(-140, 0),
        infoBoxClearance: new google.maps.Size(1, 1),
        enableEventPropagation: false,
        closeBoxURL: ''
      });
      $this.data('infowindow', infowindow);

      const $close = $('<button class="infobox__close">Close</button>');
      google.maps.event.addListener(infowindow, 'domready', () => {
        $('.infobox').append($close);
      });

      $close.click((e) => {
        e.preventDefault();
        infowindow.close();
      });

      if ($this.data('autoclose-info') === true) {
        google.maps.event.addListener(map, 'click', () => {
          infowindow.close();
        });
      }

      let marker;
      let iconMarkerFilename = '';
      const imageDir = `${window.themeUrl}assets/images/`;
      if ($this.data('marker-filename')) {
        iconMarkerFilename = imageDir + $this.data('marker-filename');
      }
      if ($this.data('marker')) {
        const markerObject = {
          position: mapCenter,
          map,
          title: $this.data('marker')
        };

        if (iconMarkerFilename) {
          markerObject.icon = iconMarkerFilename;
        }
        marker = new google.maps.Marker(markerObject);
      }

      if ($this.data('locations')) {
        const $locations = $($this.data('locations'));
        let i = 0;
        $locations.each(function eachLocations() {
          const $location = $(this);
          const lat = $location.data('lat');
          const lng = $location.data('lng');
          const markerObject = {
            position: new google.maps.LatLng(lat, lng),
            map,
            title: $this.data('title')
          };
          if ($location.data('marker-filename')) {
            markerObject.icon = imageDir + $location.data('marker-filename');
          }
          else if (iconMarkerFilename) {
            markerObject.icon = iconMarkerFilename;
          }
          marker = new google.maps.Marker(markerObject);

          marker.source = $location;
          markers.push(marker);

          google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent(this.source.clone().wrap('<div/>').parent().html());
            infowindow.open(map, this);
          });
          i++;
        });
      }

      const centerMap = function() {
        map.setCenter(mapCenter);
      };

      google.maps.event.addDomListener(window, 'resize', centerMap);
    });
  });
};

export default class GoogleMap {
  constructor() {
    this.$items = $('[data-gmap]');
    if (this.$items.length) {
      this.init();
    }
  }

  init() {
    loadJS('https://maps.googleapis.com/maps/api/js?v=3.exp&callback=googleMapLoaded');
  }
}
