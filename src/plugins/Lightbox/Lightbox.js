import $ from 'jquery';

export default class Lightbox {
  constructor() {
    this.$items = $('[data-lightbox]');
    if (this.$items.length) {
      this.init();
    }
  }

  init() {
    require(['magnific-popup'], () => {
      this.$items.each((i, el) => {
        this.setElement(el);
      });
    });
  }

  setElement(el) {
    const $this = $(el);
    const type = $this.data('lightbox-type') || 'image';
    const galleryEnabled = $this.data('gallery');

    const options = {
      type,
      // image:
      // verticalFit: true,
      gallery: {
        enabled: galleryEnabled,
        navigateByImgClick: true,
        arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
        tPrev: 'Previous (Left arrow key)',
        tNext: 'Next (Right arrow key)',
        tCounter: '<span class="mfp-counter">%curr% / %total%</span>',
        preload: [0, 1]
      },
      // For animation
      removalDelay: 300,
      mainClass: 'mfp-fade',
      closeMarkup: '<button type="button" title="%title%" class="mfp-close">Close</button>'
    };

    $this.magnificPopup(options);

    if ($this.data('delegate')) {
      options.delegate = $this.data('delegate');
    }

  }
}
