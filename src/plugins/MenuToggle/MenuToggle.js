/**
 *  Example:
 *  <button type="button" class="site-header__toggle menu-toggle hamburger hamburger--slider"
 *     aria-label="Menu" role="button" aria-controls="navigation"
 *     data-target=".site-header__nav" data-menu-toggle>
 *    <span class="hamburger-box">
 *      <span class="hamburger-inner"></span>
 *    </span>
 *  </button>
 *
 *  Remember to add an id "navigation" to the target to match aria-controls:
 *    <div class="site-header__nav" id="navigation" …
 */

import $ from 'jquery';
import Globals from 'plugins/Globals/Globals.js';

export default class MenuToggle {
  constructor() {
    this.$items = $('[data-menu-toggle]');

    if (this.$items.length) {
      this.init();
    }
  }

  init() {
    const activeClass = this.$items.data('active-class') || 'is-active';
    const bodyClass = this.$items.data('body-class') || 'is-menu-open';
    const target = this.$items.data('target') || '.site-header__menu-wrapper';
    const menuLink = this.$items.data('menu-link') || '.site-header__menu a';
    const $target = $(target);

    this.$items.click((e) => {
      e.preventDefault();
      this.$items.toggleClass(activeClass);
      const isOpen = this.$items.hasClass(activeClass);
      Globals.$body.toggleClass(bodyClass, isOpen);
      if (isOpen) {
        $target.slideDown();
      }
      else {
        $target.slideUp();
      }
    });

    Globals.$body.on('click.menu-toggle', menuLink, () => {
      this.$items.removeClass(activeClass);
      Globals.$body.removeClass(bodyClass);
      $target.slideUp();
    });
  }
}
