/**
 * Simply configure individual elements using stellar.js data-* properties.
 * @see stellar.js documentation (http://markdalgleish.com/projects/stellar.js/docs/)
 * Ex:
 * For backgrounds: <div data-stellar-background-ratio="0.5">
 */

// WARNING:
// need to change jquery.stellar.js (0.6.2), last line should be:
// }(jQuery, window, document));
//
// and not
// }(jQuery, this, document));

import Globals from 'plugins/Globals/Globals.js';
import 'vendors/bower_components/jquery.stellar/jquery.stellar.js';

export default class Parallax {
  constructor() {
    Globals.$window.stellar({
      horizontalScrolling: false,
      responsive: false
    });
  }
}
