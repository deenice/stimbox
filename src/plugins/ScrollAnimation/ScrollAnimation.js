/**
 *  Example:
 *  <h2 class="article__title"
 *    data-animation
 *    data-animation-class="article__title--visible">Title</h2>
 */

/* eslint no-new: 0 */
import $ from 'jquery';
import 'waypoints';
import Globals from 'plugins/Globals/Globals.js';
const Waypoint = window.Waypoint;

export default class ScrollAnimation {
  constructor() {
    if (Globals.$html.hasClass('csstransitions')) {
      $('[data-animation]').each((i, el) => {
        const $this = $(el);
        new Waypoint({
          element: el,
          handler: this.update,
          triggerOnce: true,
          offset: $this.data('offset') || '80%'
        });
      });
    }
  }

  update() {
    const $this = $(this.element);
    const activeClass = $this.data('animation-class') || 'visible';
    $this.addClass(activeClass);
  }
}
