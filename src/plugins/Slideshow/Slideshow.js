/**
 * This use data-slideshow so that it is possible to manually create other
 * types of slideshows if needed.
 *
 * Example:
 * <div data-slideshow> ... <div><img ...></div> ... </div>
 * Some configuration can be passed with data-* attributes on the .bxslider
 * data-fade: 'true', 'false' (default false)
 * data-arrows: 'true', 'false' (default true)
 * data-infinite: 'true', 'false' (default true)
 * data-dots: 'true', 'false' (default false)
 * data-autoplay: 'true', 'false' (default false)
 * data-autoplaySpeed: milliseconds before auto next slide (default 4000)
 *
 * example:
 * <div data-slideshow data-fade="true" data-dots="true">
*/

import $ from 'jquery';
import Globals from 'plugins/Globals/Globals.js';

export default class Slideshow {
  constructor() {
    this.$items = $('[data-slideshow]');
    if (this.$items.length) {
      this.init();
    }
  }

  init() {
    require.ensure('slick-carousel', (require) => {
      require('slick-carousel');
      this.$items.each((i, el) => {
        this.setElement(el);
      });
    });
  }

  setElement(el) {
    const $this = $(el);
    const fade = $this.data('fade') || false;
    const dots = $this.data('dots') || false;
    const infinite = $this.data('infinite') !== undefined ? $this.data('infinite') : true;
    const arrows = $this.data('arrows') !== undefined ? $this.data('arrows') : true;
    const autoplay = $this.data('autoplay') || false;
    const autoplaySpeed = $this.data('autoplaySpeed') || 4000;
    const swipe = $this.data('swipe') || true;
    const adaptiveHeight = $this.data('adaptiveHeight') || false;

    let options = {
      fade,
      dots,
      arrows,
      autoplay,
      autoplaySpeed,
      infinite,
      adaptiveHeight,
      swipe
    };
    options.useTransform = false;
    let $slick = $this.slick(options);

    Globals.$body.on('click', '[data-slick-next]', (e) => {
      e.preventDefault();
      Globals.$body.animate({ scrollTop: 0 }, 300);
      $slick.slick('slickNext');
    });

    Globals.$body.on('click', '[data-slick-prev]', (e) => {
      e.preventDefault();
      Globals.$body.animate({ scrollTop: 0 }, 300);
      $slick.slick('slickPrev');
    });

    $slick.on('beforeChange', function(event, slick, currentSlide) {
      const $slide = $(slick.$slides.get(currentSlide));
      const $timer = $slide.find('[data-timer]');
      const $audio = $slide.find('[data-audio-player]');
      const $scrollable = $('.av__scrollable');
      $scrollable.removeClass('av__scrollable--animate');
      if ($timer.length) {
        const timer = $timer.data('timer');
        timer.stop();
      }
      if($audio.length){
        const audio = $audio.data('audio-player');
        audio.stop();
      }
      $slide.find('.av__slide-image').addClass('hidden');
    });

    $slick.on('afterChange', function(event, slick, currentSlide) {
      const $slide = $(slick.$slides.get(currentSlide));
      const $timer = $slide.find('[data-timer]');
      const $audio = $slide.find('[data-audio-player]');
      if ($timer.length) {
        const timer = $timer.data('timer');
        timer.start();
      }
    });

    $slick.on('setPosition', (event, slick) => {
      const $slide = $(slick.$slides.get(slick.currentSlide));
      const $audio = $slide.find('[data-audio-player]');
      this.showScrollable(slick);
      if($audio.length){
        const audio = $audio.data('audio-player');
        audio.play();
      }
      $slide.find('.av__slide-image').removeClass('hidden');
    });

  }

  showScrollable( slick ){
    const $slide = $(slick.$slides.get(slick.currentSlide));
    const $scrollable = $('.av__scrollable');
    let windowHeight = $(window).height() - 81 - 21; // header and timer heights
    let slideHeight = $slide.find('.av__slide').height();
    if(windowHeight <= slideHeight){
      $scrollable.addClass('av__scrollable--animate');
    }
  }
}
