import $ from 'jquery';
import Globals from 'plugins/Globals/Globals.js';

export default class SmoothAnchors {
  constructor() {
    if($('.av').length){return;}
    Globals.$body.on('click.anchor', 'a[href*="#"]:not([href="#"])', function(e) {
      if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '')
       && location.hostname === this.hostname) {
        let target = $(this.hash);
        target = target.length ? target : $(`[name=${this.hash.slice(1)}]`);
        let headerHeight = $('header').height();
        if (target.length) {
          e.preventDefault();
          Globals.$htmlbody.animate({
            scrollTop: target.offset().top - 81 // height of sticky header
          }, 1000);
          return false;
        }
      }
      return true;
    });
  }
}
