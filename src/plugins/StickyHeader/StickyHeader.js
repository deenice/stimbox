// This add a 'has-scrolled' class to the body

import 'vendors/bower_components/headroom.js/dist/headroom.js';
import $ from 'jquery';
import Globals from 'plugins/Globals/Globals.js';
const Headroom = window.Headroom;

export default class StickyHeader {
  constructor() {
    const headroom = new Headroom(Globals.$body.get(0), {
      offset: 100,
      tolerance: 10,
      classes: {
        unpinned: 'has-scrolled'
      }
    });
    headroom.init();
  }
}
