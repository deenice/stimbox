/**
 * Basic use:
 * <span data-tooltip title="my tooltip content">hover tooltip</span>
 *
 * To display html content in the tooltip
 * <span data-tooltip aria-describedby="unique_id">info</span>
 * <aside id="unique_id" class="tooltip__content--hidden" data-offset-y="-30">
 *   <h2>Tooltip html title</h2>
 *   <p>lorem ipsum</p>
 * </aside>
 *
 * options:
 * - data-offset-x
 * - data-offset-y
 * - data-interactive (true or false, default to false)
 * - data-theme (class added to the tooltip, default to 'tooltipster-default')
 */

import $ from 'jquery';

export default class Tooltip {
  constructor() {
    this.$items = $('[data-tooltip]');
    if (this.$items.length) {
      this.init();
    }
  }

  init() {
    require(['../../vendors/bower_components/tooltipster/js/jquery.tooltipster.js'], () => {
      this.$items.each((i, el) => {
        this.setElement(el);
      });
    });
  }

  setElement(el) {
    const $this = $(el);
    const options = {
      contentAsHTML: true,
      animation: $this.data('animation') || 'fade',
      offsetX: $this.data('offset-x') || 0,
      offsetY: $this.data('offset-y') || 0,
      interactive: $this.data('interactive') || false,
      theme: $this.data('theme') ? $this.data('theme') : 'tooltipster-default'
    };

    const target = $this.attr('aria-describedby');
    if (target) {
      $this.data('target', $(`#${target}`));
      options.functionInit = this.onInit;
      options.functionReady = this.onReady;
      options.functionAfter = this.onAfter;
    }

    $this.tooltipster(options);
  }

  onInit() {
    const $target = $(this).data('target');
    return $target.html();
  }

  onReady() {
    const $target = $(this).data('target');
    $target.attr('aria-hidden', false);
  }

  onAfter() {
    const $target = $(this).data('target');
    $target.attr('aria-hidden', true);
  }
}
