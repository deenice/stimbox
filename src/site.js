import $ from 'jquery';
import Globals from 'plugins/Globals/Globals.js';
import Timer from 'modules/Timer/Timer.js';
import AudioPlayer from 'modules/AudioPlayer/AudioPlayer.js';
import toastr from 'toastr';

export default class Main {
  constructor() {

    this.$timers = $('[data-timer]');
    this.$timers.each((i, el) => {
      new Timer(el);
    });

    this.$audioplayers = $('[data-audio-player]');
    this.$audioplayers.each((i, el) => {
      new AudioPlayer(el);
    });

    if ($('input[type="password"]').length) {
      $('input[type="password"]').attr('placeholder', Drupal.t('Password'));
      $('input[type="password"].password-confirm').attr('placeholder', Drupal.t('Confirm password'));
    }

    $('input.form-number').attr('step', 1);

    if ($('.billing_information_default_values').length) {
      $('.billing_information_default_values span').each((i, e) => {
        var $item = $(e);
        var $input = $('input[name="' + $item.data('name') + '"]');
        if ($input.val() == '' && $item.data('value')) {
            $input.val($item.data('value'))
        }
      });
    }

    Globals.$window.scroll(this.onScroll);
    this.onScroll();

    Globals.$body.on('blur', '#edit-login-register-mail', (e) => {
      let email = $(e.target).val();
      if (email) $('input[name="login[register][name]"]').val(email);
    });

    Globals.$body.on('click', '.form__next', (e) => {
      e.preventDefault();
      let $this = $(e.target).closest('.form__next');
      $this.addClass('form__next--hidden');
      let $fieldset = $('#edit-fieldset2');
      let $actions = $('.form-actions');
      $fieldset.addClass('fieldset--visible');
      $actions.addClass('form-actions--visible');
    });

    Globals.$body.on('touchstart', '.paragraph__box-tooltip', (e) => {
      e.preventDefault();
      let $tooltip = $(e.target).closest('.paragraph__box-tooltip');
      let $tooltips = $('.paragraph__box-tooltip');
      if ($tooltip.hasClass('paragraph__box-tooltip--open')) {
        $tooltip.removeClass('paragraph__box-tooltip--open');
        return;
      } else {
        $tooltips.removeClass('paragraph__box-tooltip--open');
        $tooltip.addClass('paragraph__box-tooltip--open');
      }
    });

    if ($('.messages').length && $('.paragraph__header').length) {
      $('.messages').appendTo('.messages--container');
    }

    if ($('.messages').length) {
      if($('[data-stimbox-module]').length){
        toastr.options.positionClass = 'toast-top-right';
        toastr.options.timeOut = 3000;
        $('.messages').each((i,e) => {
          let $msg = $(e);
          let content = $msg.html();
          if($msg.hasClass('messages--status')){
            toastr.success(content);
          }
          if($msg.hasClass('messages--warning')){
            toastr.warning(content);
          }
          if($msg.hasClass('messages--info')){
            toastr.info(content);
          }
          if($msg.hasClass('messages--error')){
            toastr.error(content);
          }
        });
      } else {
        let top = $(".messages").offset().top;
        let height = $(window).height();
        if ((top - 100) >= height || true) {
          $('html, body').animate({
            scrollTop: $(".messages").offset().top - 100
          }, 1000);
        }

      }
    }

    Globals.$body.on('click', '[data-popup-close]', (e) => {
      e.preventDefault();
      $.magnificPopup.close();
    });

    if($('.av').length){
      window.onbeforeunload = function() {
        return "Êtes-vous sûr de vouloir quitter le jeu?";
      }
    }

    if($('.kint').length){
      $('header').hide();
    }

    if($('.checkout__coupons').length && $('.order-total-line__adjustment').length < 1){
      $('.checkout__coupons').appendTo('.coupon').removeClass('hidden');
    }

    if($('#edit-login').length && $('#edit-coupons')){
      $('#edit-coupons').remove();
    }

    toastr.options = {
      timeOut: 0,
      preventDuplicates: true,
      closeButton: true,
      showDuration: 200,
      closeDuration: 200
    };

    $('[data-delete-team-member]').click((e) => {
      e.preventDefault();
      let $this = $(e.target);
      let $div = $this.parents('.form-wrapper__member');
      let $confirm = confirm(Drupal.t('Do you really want to remove this member?'));
      let $form = $this.parents('form');
      if($confirm){
        $div.toggleClass('form-wrapper__member--deleted');
        $div.find('.form-text').val('');
      }
      toastr.warning('Veuillez enregistrer vos modifications.');
    });

    $('[data-edit-team]').click((e) => {
      e.preventDefault();
      let $title = $('#edit-team-infos-title');
      //let $file = $('.form-wrapper__logo');
      let title = $title.val();
      let readonly = $title.attr('readonly');
      if(readonly){
        $title.prop('readonly', false);
      } else {
        $title.prop('readonly', true);
      }
      //$file.toggleClass('hidden');
      toastr.warning('Veuillez enregistrer vos modifications.');
    });

    Globals.$body.on('click', '[data-toggle-member-sex]', (e) => {
      e.preventDefault();
      let $this = $(e.currentTarget);
      let $div = $this.parents('.form-wrapper__member');
      if($this.hasClass('icon-sex--f')){
        $this.removeClass('icon-sex--f').addClass('icon-sex--m');
        $div.find('[type="radio"][value="f"]').prop('checked', false);
        $div.find('[type="radio"][value="m"]').prop('checked', true);
      } else {
        $this.addClass('icon-sex--f').removeClass('icon-sex--m');
        $div.find('[type="radio"][value="f"]').prop('checked', true);
        $div.find('[type="radio"][value="m"]').prop('checked', false);
      }
      toastr.warning('Veuillez enregistrer vos modifications.');
    });

    // Globals.$body.on('click', '[data-add-team-member]', (e) => {
    //   e.preventDefault();
    //   let $this = $(e.currentTarget);
    //   let $clone = $('.form-wrapper__member--clone:first').clone();
    //   let numberOfMembers = $('.form-wrapper__member').get().length;
    //   $clone.find('[type="text"], [type="radio"]').each((i,e) => {
    //     let $input = $(e);
    //     let name = $input.attr('name');
    //     name = name.replace('%%index%%', numberOfMembers);
    //     $input.attr('name', name);
    //   });
    //   $clone.removeClass('form-wrapper__member--clone').appendTo('.form-wrapper__members');
    //   return false;
    // });

    $('.account__billing-form .form-no-label label').each((i,e) => {
      $(e).removeClass('visually-hidden');
    });

    Globals.$body.on('click', '.account__nav li:not(.active)', (e) => {
      e.preventDefault();
      let $this = $(e.currentTarget);
      document.location.href = $this.find('a').attr('href');
    });

    Globals.$body.on('click', '.account__nav li.active', (e) => {
      e.preventDefault();
      let $this = $(e.currentTarget);
      let $nav = $this.parents('ul');
      $nav.toggleClass('open');
    });

  }

  onScroll() {
    let scroll = Globals.$window.scrollTop();
    if (scroll > 100) {
      Globals.$body.addClass('sticky-header');
    } else {
      Globals.$body.removeClass('sticky-header');
    }
  }
}
